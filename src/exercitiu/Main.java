package exercitiu;

import java.sql.*;
import java.util.Scanner;

public class Main {

    private static Connection connect = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;
    private static String url = "jdbc:mysql://localhost:3306/facultate";
    private static String user = "root", pass = "1337";

    public static void main(String[] args) {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, user, pass);
            statement = connect.createStatement();

            Scanner scanner = new Scanner(System.in);
            boolean addStudents = true;

            while (addStudents) {
                System.out.println("Doriti sa introduceti un student? (da/nu)");
                String response = scanner.nextLine();

                if (response.equalsIgnoreCase("da")) {
                    System.out.println("Introduceti numele studentului:");
                    String nume = scanner.nextLine();

                    System.out.println("Introduceti prenumele studentului:");
                    String prenume = scanner.nextLine();

                    System.out.println("Introduceti varsta studentului:");
                    int varsta = scanner.nextInt();
                    scanner.nextLine();

                    System.out.println("Introduceti facultatea studentului:");
                    String facultate = scanner.nextLine();

                    System.out.println("Introduceti sexul studentului (F/M):");
                    String sex = scanner.nextLine();

                    String insertQuery = "INSERT INTO Persoana (Nume, Prenume, Varsta, Facultate, Sex) VALUES (?, ?, ?, ?, ?)";
                    PreparedStatement preparedStatement = connect.prepareStatement(insertQuery);
                    preparedStatement.setString(1, nume);
                    preparedStatement.setString(2, prenume);
                    preparedStatement.setInt(3, varsta);
                    preparedStatement.setString(4, facultate);
                    preparedStatement.setString(5, sex);
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                } else if (response.equalsIgnoreCase("nu")) {
                    addStudents = false;
                } else {
                    System.out.println("Optiune invalida! Va rog introduceti 'da' sau 'nu'.");
                }
            }
            
            resultSet = statement.executeQuery("SELECT * FROM Persoana");
            while (resultSet.next()) {
                int id = resultSet.getInt("Id");
                String nume = resultSet.getString("Nume");
                String prenume = resultSet.getString("Prenume");
                int varsta = resultSet.getInt("Varsta");
                String facultate = resultSet.getString("Facultate");
                String sex = resultSet.getString("Sex");
                System.out.println(id + " " + nume + " " + prenume + " " + varsta + " " + facultate + " " + sex);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}